# Switchboard Chrome Extension Release Notes

## v1.1.3
* Save environment profiles from launch website. 
* Delete/Delete all Saved environment profiles.
* Detect Tag Manager on any website.
* Display loading message while page is loading and then automatically refresh TMS detected once page has finished loading. 
* Detect Tag Managers
* Save Environments from Adobe Launch Website
* Launch to Launch Redirect Rules.
* DTM to staging Redirect
* DTM to Launch Redirect
* Tealium prod/dev/qa redirect
* Detect 404 when an environment is not built
* Add custom redirect rules
* Redirect based on custom redirect rules
* Delete/Update custom redirect rules
* Multiple TMS detect
* Disable a TMS from multiple tms foundPreviously we had to close the extension and open again to refresh the data.
